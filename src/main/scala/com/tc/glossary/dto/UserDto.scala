package com.tc.glossary.dto

import com.tc.glossary.mongo.entity.User

import scala.beans.BeanProperty

class UserDto extends MetadataDto {
  @BeanProperty
  var id: String = _
  @BeanProperty
  var email: String = _
  @BeanProperty
  var firstName: String = _
  @BeanProperty
  var lastName: String = _

  def apply(user: User) = {
    this.id = user.id
    this.email = user.email
    this.active = user.active
    this.firstName = user.firstName
    this.lastName = user.lastName
    this.createdAt = user.createdAt
    this.updatedAt = user.updatedAt
  }
}
