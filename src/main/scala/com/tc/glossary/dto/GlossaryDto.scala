package com.tc.glossary.dto

import com.tc.glossary.mongo.entity.Glossary
import com.tc.glossary.validation.{CreateGroup, UpdateGroup}
import javax.validation.constraints.{NotEmpty, NotNull}

import scala.beans.BeanProperty

class GlossaryDto extends MetadataDto {
  @BeanProperty
  @NotNull(groups = Array(classOf[UpdateGroup]))
  var id: String = _;

  @BeanProperty
  @NotNull(groups = Array(classOf[CreateGroup], classOf[UpdateGroup]))
  @NotEmpty(groups = Array(classOf[CreateGroup], classOf[UpdateGroup]))
  var glossary: String = _;

  @BeanProperty
  @NotNull(groups = Array(classOf[CreateGroup], classOf[UpdateGroup]))
  @NotEmpty(groups = Array(classOf[CreateGroup], classOf[UpdateGroup]))
  var description: String = _;

  @BeanProperty
  @NotNull(groups = Array(classOf[CreateGroup]))
  @NotEmpty(groups = Array(classOf[CreateGroup]))
  var createdByUserId: String = _;

  def apply(glossary: Glossary) = {
    this.id = glossary.id
    this.glossary = glossary.glossary
    this.active = glossary.active
    this.createdByUserId = glossary.createdByUserId
    this.description = glossary.description
    this.createdAt = glossary.createdAt
    this.updatedAt = glossary.updatedAt
  }
}
