package com.tc.glossary.dto

import scala.beans.BeanProperty

class GlossaryNotificationDto extends MetadataDto {
  @BeanProperty
  var userId: String = _
  @BeanProperty
  var notifyUser: Boolean = _
  @BeanProperty
  var glossaryId: String = _
}
