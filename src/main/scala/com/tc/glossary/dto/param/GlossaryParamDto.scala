package com.tc.glossary.dto.param

import scala.beans.BeanProperty
import java.lang
class GlossaryParamDto extends PaginationParamDto {
  @BeanProperty
  var glossaryId: Array[String] = _
  @BeanProperty
  var active: lang.Boolean = _
  @BeanProperty
  var glossary: String = _
  @BeanProperty
  var description: String = _
  @BeanProperty
  var createdByUser: String = _
}
