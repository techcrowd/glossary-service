package com.tc.glossary.dto.param

import scala.beans.BeanProperty

class PaginationParamDto {
  @BeanProperty
  var disablePagination: Boolean = _
}
