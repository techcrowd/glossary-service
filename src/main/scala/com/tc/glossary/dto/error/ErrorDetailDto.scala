package com.tc.glossary.dto.error

import com.fasterxml.jackson.annotation.JsonInclude

import scala.beans.BeanProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
class ErrorDetailDto {
  @BeanProperty var error: String = _
  @BeanProperty var `object`: Any = _
}
