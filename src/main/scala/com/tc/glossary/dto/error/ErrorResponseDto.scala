package com.tc.glossary.dto.error

import java.util

import scala.beans.BeanProperty

case class ErrorResponseDto (
   @BeanProperty `type`: String,
   @BeanProperty errors : util.List[ErrorDetailDto]
)
