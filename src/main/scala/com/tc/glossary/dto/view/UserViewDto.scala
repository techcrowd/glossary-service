package com.tc.glossary.dto.view

import com.tc.glossary.dto.UserDto

import scala.beans.BeanProperty

class UserViewDto extends UserDto {
  @BeanProperty
  var fullName: String = _
}
