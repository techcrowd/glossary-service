package com.tc.glossary.dto.view

import com.tc.glossary.dto.GlossaryDto

import scala.beans.BeanProperty

class GlossaryViewDto extends GlossaryDto {
  @BeanProperty
  var createdByUser: UserViewDto = _
}
