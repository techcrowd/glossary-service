package com.tc.glossary.dto.view

case class CountViewDto(val total: Long)
