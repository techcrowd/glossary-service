package com.tc.glossary.dto

import scala.beans.BeanProperty

class PaginatedResultsDto[T] {
  @BeanProperty
  var page: Long = 1
  @BeanProperty
  var totalRecords: Long = 0
  @BeanProperty
  var limit: Long = 10
  @BeanProperty
  var results: Array[T] = _
}
