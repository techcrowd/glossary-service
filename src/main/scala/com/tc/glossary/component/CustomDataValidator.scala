package com.tc.glossary.component

import java.text.MessageFormat
import java.util

import com.tc.glossary.dto.error.ErrorDetailDto
import com.tc.glossary.exception.DataValidationException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.util.Assert
import org.springframework.validation.{BeanPropertyBindingResult, BindingResult, FieldError, SmartValidator}

@Component
class CustomDataValidator {
  val log = LoggerFactory.getLogger(this.getClass)
  @Autowired
  private var messageSource: MessageSource = _
  @Autowired
  private var validator: SmartValidator = _

  @throws[DataValidationException]
  def validateIndicatorValueList[T](objectList: util.List[T], objectName: String, validationHints: Class[_]*): Unit = {
    Assert.isTrue(objectList != null, "objectList cannot be null")
    Assert.isTrue(!objectList.isEmpty, "objectList cannot be empty")
    Assert.isTrue(objectName != null, "objectName cannot be null")
    Assert.isTrue(!objectName.isEmpty, "objectName cannot be empty")
    val errorDetails = new util.ArrayList[ErrorDetailDto]

    for (index <- 0 until objectList.size()) {
      val bindingResult = new BeanPropertyBindingResult(objectList.get(index), s"${objectName}[${index}]")
      // spring smart validator binds the binding result with any errors
      validator.validate(objectList.get(index), bindingResult,  validationHints.map(_.asInstanceOf[Any]) : _*)
      // if any errors from binding result
      if (bindingResult.hasErrors) { // gets all the error messages
        errorDetails.add(this.getFieldErrors(objectList.get(index), bindingResult))
      }
    }

    if (errorDetails.size > 0) { // throws errors IFF errorDetails is not empty
      throw DataValidationException("Data validation exception", errorDetails)
    }
  }

  private def getFieldErrors[T](value: T, bindingResult: BindingResult): ErrorDetailDto = {
    val errorDetailDto = new ErrorDetailDto
    errorDetailDto.`object` = value
    val fieldErrors = bindingResult.getFieldErrors
    val errors = new util.ArrayList[String]

    fieldErrors.forEach {
      fieldError => errors.add(this.getMessage(fieldError))
    }

    errorDetailDto.error = String.join(", ", errors)
    errorDetailDto
  }

  private def getMessage(fieldError: FieldError): String  = {
    val fieldName = fieldError.getField.replaceAll("\\[(.*?)\\]", "")
    var message = fieldError.getDefaultMessage

    try {
      if (message.contains("{0}")) message = MessageFormat.format(message, fieldError.getRejectedValue)
      log.info("message: {}", s"${fieldError.getObjectName}.${fieldName}.${fieldError.getCode}".replaceAll("\\[(.*?)\\]", ""))
      message = messageSource.getMessage(
        s"${fieldError.getObjectName}.${fieldName}.${fieldError.getCode}".replaceAll("\\[(.*?)\\]", ""),
        if (fieldError.getRejectedValue != null) Array[AnyRef](fieldError.getRejectedValue.toString) else null,
        null
      )
    } catch {
    case e: Exception =>
      log.info("message not found for {}", fieldError.getObjectName + "." + fieldName + "." + fieldError.getCode)
    }

    message
  }
}

