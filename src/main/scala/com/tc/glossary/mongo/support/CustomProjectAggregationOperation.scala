package com.tc.glossary.mongo.support

import org.bson.Document
import org.springframework.data.mongodb.core.aggregation.{AggregationOperation, AggregationOperationContext}

// based on https://stackoverflow.com/questions/51107626/spring-data-mongodb-lookup-with-pipeline-aggregation
class CustomProjectAggregationOperation(val jsonOperation: String) extends AggregationOperation {

  override def toDocument(aggregationOperationContext: AggregationOperationContext): Document = {
    aggregationOperationContext.getMappedObject(Document.parse(jsonOperation))
  }
}
