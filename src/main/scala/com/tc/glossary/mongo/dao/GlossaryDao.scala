package com.tc.glossary.mongo.dao

import java.util.regex.Pattern

import com.tc.glossary.dto.PaginatedResultsDto
import com.tc.glossary.dto.param.GlossaryParamDto
import com.tc.glossary.dto.view.{CountViewDto, GlossaryViewDto}
import com.tc.glossary.mongo.entity.Glossary
import com.tc.glossary.mongo.support.CustomProjectAggregationOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.{Aggregation, AggregationOperation, AggregationResults}
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Repository

import scala.jdk.CollectionConverters._
import scala.collection.mutable.ListBuffer

@Repository
class GlossaryDao {
  @Autowired
  private var mongoTemplate: MongoTemplate = _

  def findGlossary(paramDto: GlossaryParamDto, pageable: Pageable): PaginatedResultsDto[GlossaryViewDto] = {
    val json = "{ $addFields: { createdByUser: { fullName: { $concat: [ '$createdByUser.firstName', \" \", '$createdByUser.lastName'  ] } } } }";

    val aggregationArray = Array(
      Aggregation.lookup("user", "id", "createdByUserId", "createdByUser"),
      Aggregation.unwind("createdByUser"),
      new CustomProjectAggregationOperation(json)
    )

    val aggregation = (builder: Array[AggregationOperation]) => Aggregation.newAggregation(
      classOf[Glossary],
      builder: _*
    )

    val criteriaList = this.filter(paramDto)

    var aggregationQueryBuilder = aggregationArray.clone()
    if (criteriaList.size > 0) {
      aggregationQueryBuilder :+= Aggregation.`match`(new Criteria().andOperator(criteriaList.toList : _*))
    }

    val aggregationCountQueryBuilder = aggregationQueryBuilder.clone() :+ Aggregation.count().as("total")

    val aggregationPaginatedAndSortedQueryBuilder = this.sortAndPaginate(paramDto, pageable, aggregationQueryBuilder)

    val count = mongoTemplate.aggregate(aggregation(aggregationCountQueryBuilder), classOf[CountViewDto])
    val values = mongoTemplate.aggregate(aggregation(aggregationPaginatedAndSortedQueryBuilder), classOf[GlossaryViewDto])

    var total = 0L
    if(count.getMappedResults.size() > 0) {
      total = count.getMappedResults.get(0).total
    }

    this.paginatedResults(paramDto.disablePagination, values, pageable, total)
  }

  private def paginatedResults(
                                disablePagination: java.lang.Boolean,
                                values: AggregationResults[GlossaryViewDto],
                                pageable: Pageable, total: Long
                              ): PaginatedResultsDto[GlossaryViewDto] = {
    val paginatedResultsDto = new PaginatedResultsDto[GlossaryViewDto]()
    paginatedResultsDto.page = pageable.getPageNumber
    paginatedResultsDto.limit = if (!disablePagination) pageable.getPageSize else 0L
    paginatedResultsDto.totalRecords = total
    paginatedResultsDto.results = values.getMappedResults.asScala.toArray
    paginatedResultsDto
  }

  private def sortAndPaginate(paramDto: GlossaryParamDto, pageable: Pageable, aggregation: Array[AggregationOperation]): Array[AggregationOperation] = {
    val sort = pageable.getSort
    var aggregationQueryBuilder = aggregation.clone()
    if (sort.isSorted) {
      aggregationQueryBuilder :+= Aggregation.sort(sort)
    }

    if(!paramDto.disablePagination) {
      aggregationQueryBuilder :+= Aggregation.limit(((pageable.getPageNumber() * pageable.getPageSize())) + pageable.getPageSize())
      aggregationQueryBuilder :+= Aggregation.skip((pageable.getPageNumber().longValue() * pageable.getPageSize().longValue()))
    }

    aggregationQueryBuilder
  }

  private def filter(paramDto: GlossaryParamDto): ListBuffer[Criteria] = {
    var criteriaList = new ListBuffer[Criteria]()
    if (paramDto.active != null) {
      criteriaList += (Criteria.where("active").is(paramDto.active))
    }

    if (paramDto.glossaryId != null) {
      criteriaList += (Criteria.where("_id").in(paramDto.glossaryId : _*))
    }

    if (paramDto.glossary != null) {
      criteriaList += (Criteria.where("glossary").regex(Pattern.compile(paramDto.glossary, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)))
    }

    if (paramDto.description != null) {
      criteriaList += (Criteria.where("description").regex(Pattern.compile(paramDto.description, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)))
    }

    if (paramDto.createdByUser != null) {
      criteriaList += new Criteria().orOperator(
        Criteria.where("createdByUser.fullName").regex(Pattern.compile(paramDto.createdByUser, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
        Criteria.where("createdByUser.email").regex(Pattern.compile(paramDto.createdByUser, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)),
        Criteria.where("createdByUserId").is(paramDto.createdByUser)
      )
    }

    criteriaList
  }
}
