package com.tc.glossary.mongo.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.tc.glossary.dto.GlossaryDto
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
class Glossary extends Metadata {
  @Id
  var id: String = _;
  @Indexed(unique = true, sparse = true)
  var glossary: String = _;
  var description: String = _;
  var createdByUserId: String = _;

  def apply(glossaryDto: GlossaryDto) = {
    this.active = glossaryDto.active
    this.id = glossaryDto.id
    this.createdByUserId = glossaryDto.createdByUserId
    this.description = glossaryDto.description
    this.createdAt = glossaryDto.createdAt
    this.updatedAt = glossaryDto.updatedAt
    this.glossary = glossaryDto.glossary
  }
}
