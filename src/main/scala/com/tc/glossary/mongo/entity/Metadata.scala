package com.tc.glossary.mongo.entity

import java.util.Date

import com.tc.glossary.validation.{CreateGroup, UpdateGroup}
import javax.validation.constraints.NotNull

import scala.beans.BeanProperty

class Metadata {
  @BeanProperty
  var createdAt: Date = new Date()
  @BeanProperty
  var updatedAt: Date = new Date()
  @BeanProperty
  @NotNull(groups = Array(classOf[CreateGroup], classOf[UpdateGroup]))
  var active: Boolean = true
}
