package com.tc.glossary.mongo.entity

import org.springframework.data.mongodb.core.index.{CompoundIndex, CompoundIndexes}
import org.springframework.data.mongodb.core.mapping.Document

@Document
@CompoundIndexes(
  Array(
    new CompoundIndex(name = "user_glossary", `def` = "{'userId' : 1, 'glossaryId': 1}")
  )
)
class GlossaryNotification extends Metadata {
  var userId: String = _
  var notifyUser: Boolean = _
  var glossaryId: String = _
}
