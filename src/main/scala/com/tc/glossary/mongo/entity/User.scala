package com.tc.glossary.mongo.entity

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
class User extends Metadata {
  @Id
  var id: String = _;
  @Indexed(unique = true, sparse = true)
  var email: String = _
  var firstName: String = _
  var lastName: String = _
}
