package com.tc.glossary.mongo.repository

import com.tc.glossary.mongo.entity.Glossary
import org.springframework.data.mongodb.repository.{MongoRepository, Query}
import org.springframework.stereotype.Repository

@Repository
trait GlossaryRepository extends MongoRepository[Glossary, String] {
  @Query("{ 'id' : ?0 }")
  def findByGlossaryId(id: String): Option[Glossary]
}
