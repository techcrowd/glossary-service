package com.tc.glossary.mongo.repository

import com.tc.glossary.mongo.entity.{GlossaryNotification}
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
trait GlossaryNotificationRepository extends MongoRepository[GlossaryNotification, String] {
}
