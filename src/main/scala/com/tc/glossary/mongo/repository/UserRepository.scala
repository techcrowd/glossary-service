package com.tc.glossary.mongo.repository

import com.tc.glossary.mongo.entity.User
import org.springframework.data.mongodb.repository.{MongoRepository, Query}
import org.springframework.stereotype.Repository

@Repository
trait UserRepository extends MongoRepository[User, String] {
  @Query("{ 'id' : ?0 }")
  def findByUserId(id: String): Option[User]

  def findIdById(id: String): Option[String]
}
