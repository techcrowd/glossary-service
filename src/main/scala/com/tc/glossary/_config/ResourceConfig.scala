package com.tc.glossary._config

import com.tc.glossary.auth.CustomRemoteTokenServices
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration, Primary}
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.oauth2.config.annotation.web.configuration.{EnableResourceServer, ResourceServerConfigurerAdapter}
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices

@Configuration
@EnableResourceServer
class ResourceConfig extends ResourceServerConfigurerAdapter {
  @Value("${security.oauth2.client.client-id}")
  var clientId: String = _
  @Value("${security.oauth2.client.client-secret}")
  var clientSecret: String = _
  @Value("${security.oauth2.resource.token-info-uri}")
  var checkTokenEndpoint: String = _
  @Value("${security.oauth2.resource.id}")
  var resourceId: String = _

  override def configure(resources: ResourceServerSecurityConfigurer): Unit = {
    resources.resourceId(resourceId).tokenServices(tokenService)
  }

  override def configure(http: HttpSecurity): Unit = {
    http.sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
      .authorizeRequests()
      .antMatchers("/api/v1/**").authenticated()
      .anyRequest().authenticated()
  }

  // TODO create custom token services
  @Primary
  @Bean
  def tokenService: ResourceServerTokenServices = {
    val tokenServices = new CustomRemoteTokenServices();
    tokenServices.setClientId(clientId);
    tokenServices.setClientSecret(clientSecret);
    tokenServices.setCheckTokenEndpointUrl(checkTokenEndpoint);

    tokenServices
  }
}
