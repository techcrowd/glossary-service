package com.tc.glossary._config

import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver

@Configuration
class WebConfig extends WebMvcConfigurer {

  override def addArgumentResolvers(resolvers: java.util.List[HandlerMethodArgumentResolver]): Unit = {
    val resolver: PageableHandlerMethodArgumentResolver = new PageableHandlerMethodArgumentResolver
    resolver.setOneIndexedParameters(true)
    resolver.setSizeParameterName("limit")
    resolver.setFallbackPageable(PageRequest.of(0, 100))

    resolvers.add(resolver)
  }
}
