package com.tc.glossary.controller

import java.util

import com.tc.glossary.auth.TokenInfo
import com.tc.glossary.component.CustomDataValidator
import com.tc.glossary.dto.{GlossaryDto, PaginatedResultsDto}
import com.tc.glossary.dto.param.GlossaryParamDto
import com.tc.glossary.dto.view.GlossaryViewDto
import com.tc.glossary.service.GlossaryService
import com.tc.glossary.validation.{CreateGroup, UpdateGroup}
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.{DeleteMapping, GetMapping, PostMapping, PutMapping, RequestBody, RequestMapping, RequestParam, RestController}

@RestController
@RequestMapping(value = Array("${app.api.url}/glossaries"))
class GlossaryController {
  val log = LoggerFactory.getLogger(this.getClass)
  @Autowired
  private var customDataValidator: CustomDataValidator = _
  @Autowired
  private var glossaryService: GlossaryService = _

  @GetMapping
  def findGlossary(
                  paramDto: GlossaryParamDto,
                  pageable: Pageable,
                  @AuthenticationPrincipal tokenInfo: TokenInfo
                ): ResponseEntity[PaginatedResultsDto[GlossaryViewDto]] = {

    new ResponseEntity[PaginatedResultsDto[GlossaryViewDto]](glossaryService.findGlossary(paramDto, pageable), HttpStatus.OK)
  }

  @PostMapping
  def createGlossary(
                  @RequestBody glossaryDto: GlossaryDto,
                  @AuthenticationPrincipal tokenInfo: TokenInfo
                ): ResponseEntity[GlossaryDto] = {

    customDataValidator.validateIndicatorValueList(
      util.Arrays.asList(glossaryDto),
      "glossaryDto",
      classOf[CreateGroup]
    )

    val createdGlossaryDto = glossaryService.createGlossary(glossaryDto);

    new ResponseEntity[GlossaryDto](createdGlossaryDto, HttpStatus.CREATED)
  }

  @PutMapping
  def updateGlossary(
                      @RequestBody glossaryDto: GlossaryDto,
                      @AuthenticationPrincipal tokenInfo: TokenInfo
                    ): ResponseEntity[GlossaryDto] = {

    customDataValidator.validateIndicatorValueList(
      util.Arrays.asList(glossaryDto),
      "glossaryDto",
      classOf[UpdateGroup]
    )

    val createdGlossaryDto = glossaryService.updateGlossary(glossaryDto);

    new ResponseEntity[GlossaryDto](createdGlossaryDto, HttpStatus.OK)
  }

  @DeleteMapping
  def deleteGlossary(
                      @RequestParam glossaryId: String,
                      @AuthenticationPrincipal tokenInfo: TokenInfo
                    ): ResponseEntity[String] = {


    val deletedGlossary = glossaryService.deleteGlossary(glossaryId);

    new ResponseEntity[String](deletedGlossary, HttpStatus.OK)
  }
}
