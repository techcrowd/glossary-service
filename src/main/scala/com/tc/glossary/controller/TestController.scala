package com.tc.glossary.controller

import com.tc.glossary.component.CustomDataValidator
import com.tc.glossary.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RestController}

@RestController
@RequestMapping(value = Array("${app.api.url}/results"))
class TestController {
  val log = LoggerFactory.getLogger(this.getClass)
  @Autowired
  private var customDataValidator: CustomDataValidator = _
  @Autowired
  private var userService: UserService = _
}
