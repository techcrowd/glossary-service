package com.tc.glossary.exception

import com.tc.glossary.dto.error.{ErrorDetailDto, ErrorResponseDto}
import org.springframework.http.{HttpHeaders, HttpStatus, MediaType, ResponseEntity}
import org.springframework.web.bind.annotation.{ControllerAdvice, ExceptionHandler}
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class AppExceptionHandler extends ResponseEntityExceptionHandler {

  private val headers = new HttpHeaders

  @ExceptionHandler(Array(classOf[Exception]))
  protected def unhandledException(ex: Exception, request: WebRequest): ResponseEntity[Object] = {
    headers.setContentType(MediaType.APPLICATION_JSON)
    var errorDetailDto = new ErrorDetailDto;
    var errorResponseDto: ErrorResponseDto = null
    if(ex.isInstanceOf[DataValidationException]) {
      errorResponseDto = ErrorResponseDto("DATA_VALIDATION_ERROR", ex.asInstanceOf[DataValidationException].errorDetailDto);
    } else if (ex.isInstanceOf[DataNotFoundException]) {
      errorDetailDto.error = ex.asInstanceOf[DataNotFoundException].message
      errorResponseDto = ErrorResponseDto("DATA_NOT_FOUND_ERROR", java.util.Arrays.asList(errorDetailDto));
    } else {
      errorDetailDto.error = ex.getMessage
      errorResponseDto = ErrorResponseDto("INTERNAL_SERVER_ERROR", java.util.Arrays.asList(errorDetailDto));
    }

    handleExceptionInternal(ex, errorResponseDto, headers, HttpStatus.INTERNAL_SERVER_ERROR, request)
  }
}
