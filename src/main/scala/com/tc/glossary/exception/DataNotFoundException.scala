package com.tc.glossary.exception

case class DataNotFoundException(message: String) extends RuntimeException(message){

}
