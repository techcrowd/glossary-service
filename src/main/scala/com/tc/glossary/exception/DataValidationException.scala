package com.tc.glossary.exception

import java.util

import com.tc.glossary.dto.error.ErrorDetailDto

case class DataValidationException(message: String, errorDetailDto: util.List[ErrorDetailDto])
  extends RuntimeException(message)
