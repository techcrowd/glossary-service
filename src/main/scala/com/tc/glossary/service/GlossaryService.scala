package com.tc.glossary.service

import java.util.Date

import com.tc.glossary.dto.{GlossaryDto, PaginatedResultsDto}
import com.tc.glossary.dto.param.GlossaryParamDto
import com.tc.glossary.dto.view.GlossaryViewDto
import com.tc.glossary.exception.DataNotFoundException
import com.tc.glossary.mongo.dao.GlossaryDao
import com.tc.glossary.mongo.entity.Glossary
import com.tc.glossary.mongo.repository.GlossaryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class GlossaryService {
  @Autowired
  private var glossaryRepository: GlossaryRepository = _

  @Autowired
  private var userService: UserService = _

  @Autowired
  private var glossaryDao: GlossaryDao = _

  def findGlossary(paramDto: GlossaryParamDto, pageable: Pageable): PaginatedResultsDto[GlossaryViewDto] = {
    glossaryDao.findGlossary(paramDto, pageable)
  }

  def findGlossary(id: String): GlossaryDto = {
    val glossaryDto = new GlossaryDto()

    glossaryDto(
      glossaryRepository
        .findByGlossaryId(id)
        .getOrElse(throw DataNotFoundException(s"Glossary not found for #${id}"))
    )

    glossaryDto
  }

  def createGlossary(glossaryDto: GlossaryDto): GlossaryDto = {
    val glossary = new Glossary

    glossaryDto.createdAt = new Date()
    glossaryDto.updatedAt = new Date()

    userService.findIdByUserId(glossaryDto.createdByUserId)

    glossary(
      glossaryDto
    )

    val createdGlossary = glossaryRepository.save(glossary)

    glossaryDto.id = createdGlossary.id

    glossaryDto
  }

  def updateGlossary(glossaryDto: GlossaryDto): GlossaryDto = {
    val glossary = glossaryRepository
      .findByGlossaryId(glossaryDto.id)
      .getOrElse(throw DataNotFoundException(s"Glossary not found for #${glossaryDto.id}"))

    glossaryDto.updatedAt = new Date()
    glossaryDto.createdAt = glossary.createdAt
    glossaryDto.createdByUserId = glossary.createdByUserId

    val glossaryToUpdate = new Glossary
    glossaryToUpdate(glossaryDto)

    glossaryRepository.save(glossaryToUpdate)

    glossaryDto
  }

  def deleteGlossary(glossaryId: String): String = {
    val glossary = glossaryRepository
      .findByGlossaryId(glossaryId)
      .getOrElse(throw DataNotFoundException(s"Glossary not found for #${glossaryId}"))

    glossary.active = false
    glossaryRepository.save(glossary);

    s"Glossary#${glossaryId} has been deleted"
  }
}
