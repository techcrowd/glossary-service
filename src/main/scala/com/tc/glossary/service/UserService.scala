package com.tc.glossary.service

import com.tc.glossary.dto.UserDto
import com.tc.glossary.exception.DataNotFoundException
import com.tc.glossary.mongo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService {
  @Autowired
  private var userRepository: UserRepository = _

  def findById(id: String): UserDto = {
    val userDto = new  UserDto;
    userDto(
      userRepository
      .findByUserId(id)
      .getOrElse(throw DataNotFoundException(s"User not found for #${id}"))
    )
    userDto
  }

  def findIdByUserId(id: String): String = {
    userRepository
      .findIdById(id)
      .getOrElse(throw DataNotFoundException(s"User not found for #${id}"))
  }
}
