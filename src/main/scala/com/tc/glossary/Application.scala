package com.tc.glossary

import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class Application

object Application extends App {
  val log = LoggerFactory.getLogger(this.getClass)
  SpringApplication.run(classOf[Application], args: _*)
}
