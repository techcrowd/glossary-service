package com.tc.glossary.auth

import java.util

import com.fasterxml.jackson.annotation.{JsonCreator, JsonIgnoreProperties, JsonInclude, JsonProperty};


@JsonCreator
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
case class TokenInfo(
 aud: util.Set[String],
 @JsonProperty("user_name")
 username: String,
 userId: String,
 authorities: util.ArrayList[String],
 fullName: String,
 scope: util.Set[String],
 active: Boolean,
 accessTokenType: String,
 exp: Long,
 jti: String,
 @JsonProperty("client_id")
 clientId: String
) extends Serializable

