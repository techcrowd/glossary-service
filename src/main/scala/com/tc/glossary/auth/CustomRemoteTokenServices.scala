package com.tc.glossary.auth

import java.io.{IOException, UnsupportedEncodingException}
import java.util.Collections
import java.util.stream.Collectors

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.commons.logging.{Log, LogFactory}
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.{HttpEntity, HttpHeaders, HttpMethod, MediaType}
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.crypto.codec.Base64
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException
import org.springframework.security.oauth2.provider.token.{AccessTokenConverter, DefaultAccessTokenConverter, ResourceServerTokenServices}
import org.springframework.security.oauth2.provider.{OAuth2Authentication, OAuth2Request}
import org.springframework.util.{Assert, LinkedMultiValueMap, MultiValueMap}
import org.springframework.web.client.{DefaultResponseErrorHandler, RestTemplate}

// code rewrite based on RemoteTokenServices
class CustomRemoteTokenServices extends ResourceServerTokenServices {

  protected val logger: Log = LogFactory.getLog(getClass)

  private var restTemplate: RestTemplate = _

  private var checkTokenEndpointUrl: String = _

  private var clientId: String = _

  private var clientSecret: String = _

  private var tokenName = "token"

  private var tokenConverter: AccessTokenConverter  = new DefaultAccessTokenConverter


  {
    this.restTemplate = new RestTemplate

    this.restTemplate.getMessageConverters
      .removeIf((m) => m.getClass.getName.equals(classOf[MappingJackson2HttpMessageConverter].getName))
    this.restTemplate.getMessageConverters
      .asInstanceOf[java.util.List[HttpMessageConverter[_]]]
      .add(messageConverter)

    this.restTemplate.asInstanceOf[RestTemplate].setErrorHandler(new DefaultResponseErrorHandler() {
      @throws[IOException]
      override // Ignore 400
      def handleError(response: ClientHttpResponse): Unit = {
        if (response.getRawStatusCode != 400) super.handleError(response)
      }
    })
  }


  def setRestTemplate(restTemplate: RestTemplate): Unit = {
    this.restTemplate = restTemplate
  }

  def setCheckTokenEndpointUrl(checkTokenEndpointUrl: String): Unit = {
    this.checkTokenEndpointUrl = checkTokenEndpointUrl
  }

  def setClientId(clientId: String): Unit = {
    this.clientId = clientId
  }

  def setClientSecret(clientSecret: String): Unit = {
    this.clientSecret = clientSecret
  }

  def setAccessTokenConverter(accessTokenConverter: AccessTokenConverter): Unit = {
    this.tokenConverter = accessTokenConverter
  }

  def setTokenName(tokenName: String): Unit = {
    this.tokenName = tokenName
  }

  @throws[AuthenticationException]
  @throws[InvalidTokenException]
  override def loadAuthentication(accessToken: String): OAuth2Authentication = {
    val formData = new LinkedMultiValueMap[String, String]
    formData.add(tokenName, accessToken)
    val headers = new HttpHeaders
    headers.set("Authorization", getAuthorizationHeader(clientId, clientSecret))

    val tokenInfo = this.getTokenInfoDetails(accessToken, headers, formData)

    val authorities: java.util.List[SimpleGrantedAuthority] = this.getAuthorities(tokenInfo)

    val request = new OAuth2Request(
      Collections.singletonMap(AccessTokenConverter.CLIENT_ID, tokenInfo.clientId),
      tokenInfo.clientId,
      authorities,
      true,
      tokenInfo.scope,
      tokenInfo.aud,
      null,
      null,
      null
    )

    val token = new UsernamePasswordAuthenticationToken(tokenInfo, "N/A", authorities)
    new OAuth2Authentication(request, token)
  }

  private def getTokenInfoDetails(accessToken: String, headers: HttpHeaders, formData: MultiValueMap[String, String]): TokenInfo = {
    var tokenInfo: TokenInfo = null

    try {
      tokenInfo = this.postForMap(checkTokenEndpointUrl, formData, headers)
      Assert.notNull(tokenInfo, "Token info should not be null")
    } catch {
      case x: Throwable =>
        logger.warn("check_token returned error: ", x)

        throw new InvalidTokenException(accessToken)
    }

    if (!tokenInfo.active) {
      logger.debug("check_token returned active attribute: " + tokenInfo.active)
      throw new InvalidTokenException(accessToken)
    }

    tokenInfo
  }

  private def getAuthorities(tokenInfo: TokenInfo): java.util.List[SimpleGrantedAuthority] = {
    var authorities: java.util.List[SimpleGrantedAuthority] = new java.util.ArrayList[SimpleGrantedAuthority]()

    if(tokenInfo.accessTokenType.equalsIgnoreCase("user")) {
      authorities = tokenInfo
        .authorities
        .asInstanceOf[java.util.List[String]]
        .stream()
        .map(x => new SimpleGrantedAuthority(x))
        .collect(Collectors.toList[SimpleGrantedAuthority])
    }

    authorities
  }

  override def readAccessToken(accessToken: String) = throw new UnsupportedOperationException("Not supported: read access token")

  private def getAuthorizationHeader(clientId: String, clientSecret: String) = {
    if (clientId == null || clientSecret == null) logger.warn("Null Client ID or Client Secret detected. Endpoint that requires authentication will reject request with 401 error.")
    val creds = String.format("%s:%s", clientId, clientSecret)
    try "Basic " + new String(Base64.encode(creds.getBytes("UTF-8")))
    catch {
      case e: UnsupportedEncodingException =>
        throw new IllegalStateException("Could not convert String")
    }
  }

  private def postForMap(path: String, formData: MultiValueMap[String, String], headers: HttpHeaders) = {
    if (headers.getContentType == null) headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)

    val result = restTemplate.exchange(
      path,
      HttpMethod.POST,
      new HttpEntity[MultiValueMap[String, String]](formData, headers),
      classOf[TokenInfo]
    ).getBody

    result
  }

  private def messageConverter : MappingJackson2HttpMessageConverter = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val messageConverter = new MappingJackson2HttpMessageConverter
    messageConverter.setObjectMapper(mapper)
    messageConverter
  }
}
