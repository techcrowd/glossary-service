# Glossary Service
Glossary service is a Scala Spring Boot (2.2.2) project. Service provides to create, update, delete and get glossary details. 

Project depends on Mongodb and uses [auth-service](https://bitbucket.org/anoj-kunes/auth-service/src/master/) to provide client to client aunthentication and protect resources via access token

## Postman API
Includes Api endpoints for Glossary service
~~~
https://www.getpostman.com/collections/eae5262d501971ce7616
~~~

## Release Notes
### 1.0.0


### Upcoming Releases

